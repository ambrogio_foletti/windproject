# -*- coding: utf-8 -*-
"""
Created on Mon Oct 31 17:13:17 2016

@author: Foletti

This reads the wins turbine XTF and stores basic turbine data to a sqlite DB
"""

import sqlite3
import xmltodict
import pyproj
import urllib2
import json
import math
import requests

wgs84 = pyproj.Proj("+init=EPSG:4326")
lv03 = pyproj.Proj("+init=EPSG:21781")
lv95 = pyproj.Proj("+init=EPSG:2056")
cartoUsername = 'yourCartoName'
cartoAPI = 'yourCartoAPIKey'


with open('./data/Windenergyplants 2016-02-29.xtf') as fd:
    doc = xmltodict.parse(fd.read())

turbines = doc['TRANSFER']['DATASECTION']['Windenergyplants_V1.Plant']\
    ['Windenergyplants_V1.Plant.Turbine']

conn = sqlite3.connect('wind.sqlite')
conn.text_factory = str
cur = conn.cursor()
year = 365

cur.execute('''DROP TABLE IF EXISTS wind_model ''')

cur.execute('''CREATE TABLE IF NOT EXISTS wind_model
    (
    `tid`	TEXT NOT NULL,
    `diameter`	INTEGER NOT NULL,
    `cutInSpeed`	INTEGER NOT NULL,
    `ratedSpeed`	INTEGER NOT NULL,
    `cutOutSpeed`	INTEGER NOT NULL,
    `hubHeight`	INTEGER NOT NULL,
    `coordx`	TEXT NOT NULL,
    `coordy`	TEXT NOT NULL,
    `layer`	TEXT NOT NULL,
    `v_mean`	NUMERIC NOT NULL,
    `k`	NUMERIC NOT NULL,
    `A`	NUMERIC NOT NULL,
    `sumProb`	NUMERIC NOT NULL,
    `days`	NUMERIC NOT NULL,
    PRIMARY KEY(tid)
    )''')

for i in range(0, len(turbines)):

    tid = turbines[i]['@TID']
    coordx, coordy = pyproj.transform(lv95, lv03, turbines[i]['position']['COORD']['C1'], turbines[i]['position']['COORD']['C2'])
    hubHeight = turbines[i]['hubHeight']
    if int(hubHeight) == 0:
        print 'Hub value is ', hubHeight, '. Discarding'
        continue
    diameter = turbines[i]['diameter']
    cutInSpeed = turbines[i]['cutInSpeed']
    if int(cutInSpeed) == 0:
        print 'cutInSpeed value is ', cutInSpeed, '. Discarding'
        continue
    cutOutSpeed = turbines[i]['cutOutSpeed']
    if int(cutOutSpeed) == 0:
        print 'cutOutSpeed value is ', cutOutSpeed, '. Discarding'
        continue
    ratedSpeed = turbines[i]['ratedSpeed']

    layer = 'ch.bfe.windenergie-geschwindigkeit_h' + str(min([50, 75, 100, 125, 150], key=lambda x:abs(x-int(hubHeight))))

    apiCall = 'https://api3.geo.admin.ch/rest/services/all/MapServer/identify'\
    '?geometry=' + str(coordx) + ',' + str(coordy) + '&geometryFormat=geojson'\
    '&geometryType=esriGeometryPoint&imageDisplay=1920,537,96'\
    '&lang=en&layers=all:' + layer + '&mapExtent=400000,75000,800000,250000'\
    '&returnGeometry=false&tolerance=1'

    response = urllib2.urlopen(apiCall)
    data = json.load(response)

    k = float(data['results'][0]['properties']['wei_k'])
    A = float(data['results'][0]['properties']['wei_a'])
    v_mean = data['results'][0]['properties']['v_mean']

    sumProb = 0.0
    for speed in range(int(cutInSpeed), int(cutOutSpeed)+1):
        sumProb = sumProb + ( (k/A) * math.pow((speed/A),(k-1)) * math.pow(math.e,-math.pow((speed/A),k)) )
    days = int(365 * sumProb)

    cur.execute('''INSERT OR IGNORE INTO wind_model (tid, diameter, cutInSpeed, ratedSpeed, cutOutSpeed, hubHeight, coordx, coordy, layer, v_mean, k, A, sumProb, days) 
        VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )''', (tid, diameter, cutInSpeed, ratedSpeed, cutOutSpeed, hubHeight, coordx, coordy, layer, v_mean, k, A, sumProb, days))

    wsg84x, wsg84y = pyproj.transform(lv03, wgs84, coordx, coordy)

    apiUrl = 'https://' + cartoUsername + '.carto.com/api/v2/sql'
    param = {'q': 'INSERT INTO wind_model (tid, diameter, cutinspeed, ratedspeed,'\
    'cutoutspeed, hubheight, coordx, coordy, layer, v_mean, k, a, sumprob,'\
    'days, the_geom) VALUES (\'' + tid + '\',' + str(diameter) + ',' + str(cutInSpeed) + ',' + str(ratedSpeed) + ',' + str(cutOutSpeed) + ',' + str(hubHeight) + ',' + str(coordx) + ',' + str(coordy) + ',\'' + layer + '\',' + str(v_mean) + ',' + str(k) + ',' + str(A) + ',' + str(sumProb) + ',' + str(days) + ',ST_SetSRID(ST_Point(' + str(wsg84x) + ',' + str(wsg84y) + '),4326))', 'api_key': cartoAPI}

    r = requests.post(apiUrl, param)
    print(r.status_code, r.reason)

    print 'Done ' + str(1+i) + ' of ' + str(len(turbines)) + ' turbines'

conn.commit()
cur.close()
