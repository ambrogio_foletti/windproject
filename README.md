# Welcome

Welcome to my windproject repo!

Please remember that, even if I'm using real world data for this project, the whole has absolutely NO VALUE from a scientific point of view. The analysis (not to speak about the code...) is too rough and should be viewed as a roughly estimated qualitative result.

## The project in short

What I'm doing here is to compare the wind turbines in Switzerland with the modeled wind characteristics at their position.
Every turbine has specific cutIn and cutOut wind speeds, which are the turbine operating boundaries. With the modeled wind characteristics it's possible to compute the cumulated probability of a wind within those boundaries at the turbine position.
A turbine with a high cumulated probability is then considered to be "well placed" with respect to its technical specification and positioning.

## Where to fetch the data

This project relies on open data only. Both the turbine data and the wind model data can be viewed on the Swiss geoportal [map.geo.admin.ch](https://s.geo.admin.ch/6f68ca8cca).
You can download both the turbine and model data from [data.geo.admin.ch](https://data.geo.admin.ch/), but for this project I decided to download directly only the turbine data and to use the [official API](http://api3.geo.admin.ch/) to fetch the wind model data for each turbine.
If you look attentively at the wind data, you will see that there are five datasets, each for a different altitude (50 to 150 meters, in 25m steps). For every turbine I used the specified hub height to choose the "best" wind data layer from which to pull the model information.

## The code

**The code is a mess.** I know it.
I'm not a programmer and my main aim was to have something functional in the shortest time, so that mess is the result.
I would like to change the code completely and have something much more polished, but for now you will have to cope with my bad code.
Basically, what I do is to read the turbines XML and extract some data, then use this data to perform API requests to get the wind model data and write it all down.
Writing is at the moment done to two very separate targets. The first is a sqlite database, while the second are API calls to my [Carto](https://carto.com/) database in order to visualize the dataset (see it [in action!](https://afoletti.carto.com/viz/b52bbee2-a464-11e6-8adf-0ecd1babdde5/map) ).
Sooner or later I'll change the code to be able to choose between the two and parametrize the whole correctly.